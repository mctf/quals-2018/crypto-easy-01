import os

from PIL import Image
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend


def encrypt(key, iv, data):
    cipher = Cipher(algorithms.AES(key), modes.CTR(iv),
                    backend=default_backend())
    encryptor = cipher.encryptor()
    return encryptor.update(data) + encryptor.finalize()


def main():
    rnd = os.urandom(32)

    img1 = Image.open('data/1.png')
    enc1 = encrypt(rnd[:16], rnd[16:], img1.tobytes())
    Image.frombytes('RGB', (640, 320), enc1).save('1.enc.png')

    img2 = Image.open('data/2.png')
    enc2 = encrypt(rnd[:16], rnd[-16:], img2.tobytes())
    Image.frombytes('RGB', (640, 320), enc2).save('2.enc.png')


if __name__ == '__main__':
    main()
