# Description

Here are two encrypted files ([one](https://gitlab.com/mctf/quals-2018/crypto-easy/raw/master/1.enc.png), [two](https://gitlab.com/mctf/quals-2018/crypto-easy/raw/master/2.enc.png))
and [script](https://gitlab.com/mctf/quals-2018/crypto-easy/blob/master/encrypt.py) by which they was encrypted. Let's see what you can do with this!

# Writeup

<details>
<summary>Click to expand</summary>

Given files are PNG images with encrypted pixels data (3 bytes per pixel).
Pixels are encrypted using AES in CTR mode with same key and IV which means
they are XORed with same sequences of bytes. So, by XORing encrypted pixels of the first image
with encrypted pixels of the second we'll remove same AES CTR sequence and get just XOR of pixels
of original unencrypted images:

```
E(pixels_1) = pixels_1 ^ gamma
E(pixels_2) = pixels_2 ^ gamma
E(pixels_1) ^ E(pixels_2) = pixels_1 ^ pixels_2
```

Here is the [solution](https://gitlab.com/mctf/quals-2018/crypto-easy/blob/master/solve.py).
And here is the [result image](https://gitlab.com/mctf/quals-2018/crypto-easy/raw/master/result.png).

</details>
