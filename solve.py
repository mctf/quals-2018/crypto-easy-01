from PIL import Image


def main():
    c1 = Image.open('1.enc.png').tobytes()
    c2 = Image.open('2.enc.png').tobytes()

    c = [c1 ^ c2 for (c1, c2) in zip(c1, c2)]
    cb = b''.join(map(lambda x: bytes([x]), c))

    img = Image.frombytes('RGB', (640, 320), cb)
    img.save('result.png', 'PNG')


if __name__ == '__main__':
    main()
